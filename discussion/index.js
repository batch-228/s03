console.log("Hello World!");

// we define classes with the use of the keyword "class" and a set of {}. Naming convention dictates that class names should begin with an uppercase letter.


class Student {

    // constructor keyword allows us to pass arguments when we instantiate objects from classes
    constructor(name, email, grades) {
        this.name = name;
        this.email = email;
        this.gradeAve = undefined
        this.passed = undefined
        this.passedWithHonors = undefined
        this.grades = studentGrades();


        function studentGrades() {
            if (grades.length > 4 || grades.length < 4 ) {
                return undefined
            } else {
                let checkNumRange = grades.every(elem => {
                    return elem >= 0 && elem <= 100
                });

                if (checkNumRange) {
                    return grades
                }
                return undefined;
            }
        }
    }
    logIn(){
    	console.log(`${this.email} has logged in`);
    	return this
    }

    logOut(){
    	console.log(`${this.email} has logged out`);
    	return this
    }

    listGrades() {
		console.log(`${this.name} grades are ${this.grades}`)
		return this
	}

	computeAve () {
	
		let sum = this.grades.reduce((a,b)=> a + b)/ this.grades.length;
		this.gradeAve = sum;
		return this

	}

	willPass(){
		let isPass;
		
		if(this.gradeAve === undefined){
			isPass = undefined
		}
		if(this.gradeAve < 85){
			isPass = false
		}
		if(this.gradeAve >= 90){
			isPass = true
		}
		this.passed = isPass
		return this

	}

	willPassWithHonors () {

		let isWithHonor;
			
			if(this.passed === undefined){
			isWithHonor = undefined
			}

			if(this.passed === true){
				isWithHonor = true
			}else{
				isWithHonor = false
			}
	
		this.passedWithHonors = isWithHonor
		return this
	} 

}


let studentOne = new Student("John", "john@mail.com", [89, 84, 78, 88]);
let studentTwo = new Student("Joe", "joe@mail.com", [78, 82, 79, 85]);
let studentThree = new Student("Jane", "jane@mail.com", [87, 89, 91, 93]);
let studentFour = new Student("Jessie", "jessie@mail.com", [91, 89, 92, 93]);


/*

1. What is the blueprint where objects are created from?
ans: class

2. What is the naming convention applied to classes?
ans: uppercase letters

3. What keyword do we use to create objects from a class?
ans: new 

4. What is the technical term for creating an object from a class?
ans: instantiation

5. What class method dictates HOW objects will be created from that class?
ans: constructor()

*/


/*
Function Coding 

1. Define a grades property in our Student class that will accept an array of 4 numbers ranging from 0 to 100 to be its value. If the argument used does not satisfy all the conditions given, this property will be set to undefined instead.

2. Instantiate all four students from the previous session from our Student class. Use the same values as before for their names, emails, and grades.

*/
console.log(studentOne);
console.log(studentTwo);
console.log(studentThree);
console.log(studentFour);



/*
Activity 2

Quiz:
1. Should class methods be included in the class constructor?
ans: no

2. Can class methods be separated by commas?
ans: no

3. Can we update an object’s properties via dot notation?
ans: yes

4. What do you call the methods used to regulate access to an object’s properties?
ans: dot notation


5. What does a method need to return in order for it to be chainable?
ans: return this 
*/

// Function coding:
// Modify the Student class to allow the willPass() and willPassWithHonors() methods to be chainable.
